package com.example.uniqueid.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;

import androidx.core.app.ActivityCompat;

public class DataUtil {

    private static DataUtil mDataUtil;

    private DataUtil() {
    }

    public static DataUtil getInstance() {
        if (mDataUtil == null) {
            mDataUtil = new DataUtil();
        }
        return mDataUtil;
    }

    @SuppressLint("HardwareIds")
    public String getUniqueDeviceNo(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) ==
                PackageManager.PERMISSION_GRANTED) {
            if (telephonyManager != null) {
                return telephonyManager.getDeviceId();
            }
        }
        return  Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }
}
