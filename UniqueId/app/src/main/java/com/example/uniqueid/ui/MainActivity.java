package com.example.uniqueid.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import com.example.uniqueid.R;
import com.example.uniqueid.util.DataUtil;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupTextView();
    }

    private void setupTextView() {
        TextView uniqueIdText = findViewById(R.id.unique_id_text);
        String uniqueId = getString(R.string.unique_id_msg) + "\n" +
                DataUtil.getInstance().getUniqueDeviceNo(this);
        uniqueIdText.setText(uniqueId);
    }
}
